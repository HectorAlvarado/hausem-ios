//
//  section2Cell.swift
//  Hausem
//
//  Created by Hector on 04/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class section2Cell: UITableViewCell {
    
    @IBOutlet weak var titleSection2: UILabel!
    @IBOutlet weak var houseButton: UIButton!
    @IBOutlet weak var departmentButton: UIButton!
    @IBOutlet weak var terrainButton: UIButton!
    @IBOutlet weak var houseLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var terrainLabel: UILabel!
    
    @IBOutlet weak var widthConstraintButtons: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintButtons: NSLayoutConstraint!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            heightConstraintButtons.constant = 80
            widthConstraintButtons.constant = 80
            titleSection2.font = titleSection2.font.withSize(15)
            houseLabel.font = houseLabel.font.withSize(15)
            departmentLabel.font = departmentLabel.font.withSize(15)
            terrainLabel.font = terrainLabel.font.withSize(15)
        }
        else if screenHeight == 736 {
            heightConstraintButtons.constant = 90
            widthConstraintButtons.constant = 90
            titleSection2.font = titleSection2.font.withSize(17)
            houseLabel.font = houseLabel.font.withSize(17)
            departmentLabel.font = departmentLabel.font.withSize(17)
            terrainLabel.font = terrainLabel.font.withSize(17)
        }
    }

}
