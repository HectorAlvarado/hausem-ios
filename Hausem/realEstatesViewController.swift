//
//  realEstatesViewController.swift
//  Hausem
//
//  Created by Hector on 26/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class realEstatesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var realEstatesTableView: UITableView!
    
    //Header
    @IBOutlet weak var realEstatesLabel: UILabel!
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var goToSearchButton: UIButton!
    var productIdSelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realEstatesName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.realEstatesTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! customCellRealEstates
        if realEstatesIcon[indexPath.row] != "" {
            let url = URL(string: realEstatesIcon[indexPath.row])
            cell.realEstateLogo.af_setImage(withURL: url!, placeholderImage: nil, filter: nil)
        } else {
            cell.realEstateLogo.image = UIImage(named:"logo_default.png")
        }
        cell.realEstateName.text = realEstatesName[indexPath.row]
        cell.realEstateAddress.text = realEstatesAddress[indexPath.row]
        cell.realEstateProducts.text = realEstatesProducts[indexPath.row]
        
        return cell
    }
    
    var valueToPass:String!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        productIdSelected = indexPath.row
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToProducts", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToProducts") {
            print("Enter to segue goToProducts")
            let viewController = segue.destination as! realEstateProductsViewController
            viewController.idRealEstate = productIdSelected
        }
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 568 {
            realEstatesLabel.font = realEstatesLabel.font.withSize(18)
        }
        else if screenHeight == 667 {
            realEstatesLabel.font = realEstatesLabel.font.withSize(20)
        }
        else if screenHeight == 736 {
            realEstatesLabel.font = realEstatesLabel.font.withSize(22)
        }
    }
    
    @IBAction func goToSearchButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToSearch", sender: self)
        }
    }

}
