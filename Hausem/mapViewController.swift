//
//  mapViewController.swift
//  Hausem
//
//  Created by Hector on 28/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit
import MapKit

class mapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    //Real estate
    @IBOutlet weak var realEstateLogo: UIImageView!
    @IBOutlet weak var realEstateName: UILabel!
    @IBOutlet weak var realEstateAddress: UILabel!
    @IBOutlet weak var realEstateGoToProfileLabel: UILabel!
    @IBOutlet weak var goToProfile: UIButton!
    
    //Header
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var propietiesLabel: UILabel!
    @IBOutlet weak var goBackButton: UIButton!
    
    var idRealEstate = 0
    var realEstateInfo: NSDictionary!
    var latitudeArray = [CLLocationDegrees]()
    var longitudeArray = [CLLocationDegrees]()
    var titleArray = [String]()
    var addressArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        realEstateInfo = searchResults[idRealEstate] as? NSDictionary
        loadProfileInfo()
        self.mapView.delegate = self
        loadPinsOnMap()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        realEstateLogo.layer.borderWidth = 1.0
        realEstateLogo.layer.masksToBounds = false
        realEstateLogo.layer.borderColor = UIColor.white.cgColor
        realEstateLogo.layer.cornerRadius = realEstateLogo.frame.size.width/2
        realEstateLogo.clipsToBounds = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.mapView.mapType = MKMapType.hybrid
        self.mapView.mapType = MKMapType.standard
        self.mapView.showsUserLocation = false
        self.mapView.delegate = nil
        self.mapView.removeFromSuperview()
        self.mapView = nil
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let reuseId = "test"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.image = UIImage(named:"hausem_pin.png")
            anView!.canShowCallout = true
            anView!.frame.size = CGSize(width: 50.0, height: 65.0)
        }
        else {
            anView!.annotation = annotation
        }
        return anView
    }
    
    func loadPinsOnMap() {
        for (index, value) in latitudeArray.enumerated() {
            print(value)
            let lat:CLLocationDegrees = latitudeArray[index]
            let long:CLLocationDegrees = longitudeArray[index]
            let latDelta:CLLocationDegrees = 0.01
            let longDelta:CLLocationDegrees = 0.01
            
            let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
            let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
            let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            
            mapView.setRegion(region, animated: true)
            
            let information = MKPointAnnotation()
            information.coordinate = location
            information.title = titleArray[index]
            information.subtitle = addressArray[index]
            
            mapView.addAnnotation(information)
        }
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 667 {
            propietiesLabel.font = propietiesLabel.font.withSize(20)
            realEstateName.font = realEstateName.font.withSize(17)
            realEstateAddress.font = realEstateAddress.font.withSize(15)
            realEstateGoToProfileLabel.font = realEstateGoToProfileLabel.font.withSize(17)
        }
        else if screenHeight == 736 {
            propietiesLabel.font = propietiesLabel.font.withSize(22)
            realEstateName.font = realEstateName.font.withSize(18)
            realEstateAddress.font = realEstateAddress.font.withSize(16)
            realEstateGoToProfileLabel.font = realEstateGoToProfileLabel.font.withSize(17)
        }
    }
    
    @IBAction func goToProfile(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToRealEstateDetails2", sender: self)
        }
    }
    
    @IBAction func goBackButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToRealEstateProducts", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToRealEstateProducts") {
            print("Enter to segue goToRealEstateProducts")
            let viewController = segue.destination as! realEstateProductsViewController
           viewController.idRealEstate = idRealEstate
        }
        if (segue.identifier == "goToRealEstateDetails2") {
            print("Enter to segue goToRealEstateDetails2")
            let viewController = segue.destination as! realEstateDetailViewController
            viewController.idRealEstate = idRealEstate
        }
    }
    
    func loadProfileInfo() {
        if let imageRealEstate = realEstateInfo!["inmo_main_pic"] as? String {
            let imageEncoding = imageRealEstate.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imageEncoding)
            realEstateLogo.af_setImage(withURL: url!, placeholderImage: nil, filter: nil)
        }
        if let nameRealEstate = realEstateInfo!["inmo_name"] as? String {
            realEstateName.text = nameRealEstate
        }
        if let addressRealEstate = realEstateInfo!["address"] as? String {
            realEstateAddress.text = addressRealEstate
        }
        if let products = realEstateInfo!["_products"] as? NSArray {
            for product in (products as? [[String:Any]])! {
                var latitude = product["product_latitude"] as! CLLocationDegrees
                var longitude = product["product_longitude"] as! CLLocationDegrees
                let title = product["product_title"] as! String
                let address = product["product_address"] as! String
                latitude = latitude / 10000000
                longitude = longitude / 10000000
                latitudeArray.append(latitude)
                longitudeArray.append(longitude)
                titleArray.append(title)
                addressArray.append(address)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
