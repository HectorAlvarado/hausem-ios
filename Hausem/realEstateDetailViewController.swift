//
//  realEstateDetailViewController.swift
//  Hausem
//
//  Created by Hector on 01/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class realEstateDetailViewController: UIViewController {
    
    //Header
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var realEstateDetailLabel: UILabel!
    @IBOutlet weak var goBackButton: UIButton!
    
    //Body labels
    @IBOutlet weak var realEstateLogo: UIImageView!
    @IBOutlet weak var realEstateName: UILabel!
    @IBOutlet weak var realEstateAddress: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var productsLabel: UILabel!
    @IBOutlet weak var seeInfoButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var heightConstraintInfoButton: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintInfoButton: NSLayoutConstraint!
    
    //info view
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var titleInfoLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailLabel2: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneLabel2: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var heightConstraintsSocialNetworks: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintsSocialNetworks: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintsSocialNetworks: NSLayoutConstraint!
    @IBOutlet weak var callToRealEstateButton: UIButton!
    var facebookURL = "", twitterURL = "", websiteURL = ""
    var phoneToCall = ""
    
    var idRealEstate = 0
    var realEstateInfo: NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        realEstateInfo = searchResults[idRealEstate] as? NSDictionary
        if screenHeight != 480 {
            realEstateLogo.layer.borderWidth = 1.0
            realEstateLogo.layer.masksToBounds = false
            realEstateLogo.layer.borderColor = UIColor.white.cgColor
            realEstateLogo.layer.cornerRadius = realEstateLogo.frame.size.width/2
            realEstateLogo.clipsToBounds = true
        }
        loadRealEstateInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //topMostController()
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        infoView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("[ViewController] Prepare for segue")
        if (segue.identifier == "goToRealEstateProducts3") {
            print("Enter to segue goToRealEstateProducts3")
            let viewController = segue.destination as! realEstateProductsViewController
            viewController.idRealEstate = idRealEstate
        }
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 667 {
            realEstateDetailLabel.font = realEstateDetailLabel.font.withSize(20)
            realEstateName.font = realEstateName.font.withSize(19)
            realEstateAddress.font = realEstateAddress.font.withSize(15)
            descriptionTextView.font = descriptionTextView.font!.withSize(15)
            productsLabel.font = productsLabel.font.withSize(15)
            phoneLabel.font = phoneLabel.font.withSize(16)
            emailLabel.font = emailLabel.font.withSize(16)
            acceptButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            
            infoLabel.font = infoLabel.font.withSize(17)
            heightConstraintInfoButton.constant = 45
            widthConstraintInfoButton.constant = 160
            
            titleInfoLabel.font = titleInfoLabel.font.withSize(17)
            emailLabel.font = emailLabel.font.withSize(16)
            emailLabel2.font = emailLabel2.font.withSize(16)
            phoneLabel.font = phoneLabel.font.withSize(16)
            phoneLabel2.font = phoneLabel2.font.withSize(16)
        }
        else if screenHeight == 736 {
            realEstateDetailLabel.font = realEstateDetailLabel.font.withSize(22)
            realEstateName.font = realEstateName.font.withSize(20)
            realEstateAddress.font = realEstateAddress.font.withSize(16)
            descriptionTextView.font = descriptionTextView.font!.withSize(16)
            productsLabel.font = productsLabel.font.withSize(16)
            phoneLabel.font = phoneLabel.font.withSize(17)
            emailLabel.font = emailLabel.font.withSize(17)
            acceptButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            
            infoLabel.font = infoLabel.font.withSize(17)
            heightConstraintInfoButton.constant = 50
            widthConstraintInfoButton.constant = 175
            
            titleInfoLabel.font = titleInfoLabel.font.withSize(19)
            emailLabel.font = emailLabel.font.withSize(18)
            emailLabel2.font = emailLabel2.font.withSize(18)
            phoneLabel.font = phoneLabel.font.withSize(18)
            phoneLabel2.font = phoneLabel2.font.withSize(18)
            heightConstraintsSocialNetworks.constant = 60
            widthConstraintsSocialNetworks.constant = 60
            leadingConstraintsSocialNetworks.constant = 45
        }
    }
    
    @IBAction func seeInfoButton(_ sender: UIButton) {
        self.infoView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.infoView.fadeIn()
        })
        infoView.isHidden = false
    }
    
    @IBAction func showFTWButton(_ sender: UIButton) {
        switch sender {
        case facebookButton:
            if facebookURL != "" {
                facebookURL = facebookURL.replacingOccurrences(of: "http://", with: "")
                if let url = URL(string: "http://\(facebookURL)"){
                    UIApplication.shared.openURL(url)
                }
            } else {
                _ = SweetAlert().showAlert("Facebook NO disponible!", subTitle: "La página de facebook no está disponible.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
            }
        case twitterButton:
            if twitterURL != "" {
                twitterURL = twitterURL.replacingOccurrences(of: "http://", with: "")
                if let url = URL(string: "http://\(twitterURL)"){
                    UIApplication.shared.openURL(url)
                }
            } else {
                _ = SweetAlert().showAlert("Twitter NO disponible!", subTitle: "La página de twitter no está disponible.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
            }
        case websiteButton:
            if websiteURL != "" {
                websiteURL = websiteURL.replacingOccurrences(of: "http://", with: "")
                if let url = URL(string: "http://\(websiteURL)"){
                    UIApplication.shared.openURL(url)
                }
            } else {
                _ = SweetAlert().showAlert("Página web NO disponible!", subTitle: "La página web no está disponible.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
            }
        default:
            break
        }
    }
    
    @IBAction func acceptButton(_ sender: UIButton) {
        self.infoView.fadeOutCalls(completion: {
            (finished: Bool) -> Void in
            self.infoView.fadeIn()
        })
        delay(0.6){
            self.infoView.isHidden = true
        }
    }
    
    @IBAction func goBackButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToRealEstateProducts3", sender: self)
        }
    }
    
    @IBAction func callToRealEstateButton(_ sender: UIButton) {
        if let url = URL(string: "tel://\(phoneToCall)") {
            UIApplication.shared.openURL(url)
        }
    }

    func loadRealEstateInfo() {
        if let imageRealEstate = realEstateInfo!["inmo_main_pic"] as? String {
            let imageEncoding = imageRealEstate.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imageEncoding)
            realEstateLogo.af_setImage(withURL: url!, placeholderImage: nil, filter: nil)
        }
        if let nameRealEstate = realEstateInfo!["inmo_name"] as? String {
            realEstateName.text = nameRealEstate
        }
        if let addressRealEstate = realEstateInfo!["address"] as? String {
            realEstateAddress.text = addressRealEstate
        }
        if let descriptionRealEstate = realEstateInfo!["description"] as? String {
            descriptionTextView.text = descriptionRealEstate
        }
        if let products = realEstateInfo!["_products"] as? NSArray {
            let totalProducts = products.count
            productsLabel.text = "\(totalProducts) propiedad(es)"
        }
        if let phoneRealEstate = realEstateInfo!["phone"] as? NSNumber {
            
            let nfPhone = NumberFormatter()
            nfPhone.numberStyle = .none
            let sPhone = nfPhone.string(from: phoneRealEstate)
            phoneLabel2.text = sPhone
            phoneToCall = sPhone!
        }
        if let emailRealEstate = realEstateInfo!["email"] as? String {
            emailLabel2.text = emailRealEstate
        }
        if let facebookRealEstate = realEstateInfo!["facebook_url"] as? String {
            facebookURL = facebookRealEstate
        } else {
            facebookURL = ""
        }
        if let twitterRealEstate = realEstateInfo!["twitter_url"] as? String {
            twitterURL = twitterRealEstate
        } else {
            twitterURL = ""
        }
        if let webRealEstate = realEstateInfo!["web_url"] as? String {
            websiteURL = webRealEstate
        } else {
            websiteURL = ""
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
