//
//  section1Cell.swift
//  Hausem
//
//  Created by Hector on 04/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class section1Cell: UITableViewCell {

    @IBOutlet weak var titleSection1: UILabel!
    @IBOutlet weak var rentButton: UIButton!
    @IBOutlet weak var saleButton: UIButton!
    @IBOutlet weak var rentLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    
    @IBOutlet weak var heightConstraintButtons: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintButtons: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintButtons: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            leadingConstraintButtons.constant = 70
            heightConstraintButtons.constant = 80
            widthConstraintButtons.constant = 80
            titleSection1.font = titleSection1.font.withSize(15)
            rentLabel.font = rentLabel.font.withSize(15)
            saleLabel.font = saleLabel.font.withSize(15)
        }
        else if screenHeight == 736 {
            leadingConstraintButtons.constant = 80
            heightConstraintButtons.constant = 90
            widthConstraintButtons.constant = 90
            titleSection1.font = titleSection1.font.withSize(16)
            rentLabel.font = rentLabel.font.withSize(16)
            saleLabel.font = saleLabel.font.withSize(16)
        }
    }

}
