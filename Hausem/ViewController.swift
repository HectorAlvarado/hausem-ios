//
//  ViewController.swift
//  Hausem
//
//  Created by Hector on 13/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imageSliderVC:TNImageSliderViewController!
    @IBOutlet weak var recentsContainerView: UIView!
    
    //recents container's constraints
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var newConstraint:  NSLayoutConstraint!
    var constantTopUp: CGFloat = 0
    var constantTopDown: CGFloat! = 570
    @IBOutlet weak var widthRecentIconConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightRecentIconConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingRecentIconConstraint: NSLayoutConstraint!
    var returnFromProductDetail = false
    
    //Labels body1
    @IBOutlet weak var headerLabel1: UILabel!
    @IBOutlet weak var headerLabel2: UILabel!
    @IBOutlet weak var departmentsLabel: UILabel!
    @IBOutlet weak var housesLabel: UILabel!
    @IBOutlet weak var terrainsLabel: UILabel!
    @IBOutlet weak var houseIcon: UIImageView!
    @IBOutlet weak var terrainIcon: UIImageView!
    @IBOutlet weak var deptoIcon: UIImageView!
    
    //Recents Variables
    @IBOutlet weak var recentsButton: UIButton!
    @IBOutlet weak var recentsLabel: UILabel!
    @IBOutlet weak var hdtRecentsButton: UIButton!
    
    //Search button Constraints
    @IBOutlet weak var heigthSearchButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthSearchButtonConstraint: NSLayoutConstraint!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("[ViewController] Prepare for segue")
        if( segue.identifier == "seg_imageSlider" ){
            imageSliderVC = segue.destination as! TNImageSliderViewController
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        startController = self
        print("[ViewController] View did load")
        let image1 = UIImage(named: "image-1")
        let image2 = UIImage(named: "image-2")
        let image3 = UIImage(named: "image-3")
        
        if let image1 = image1, let image2 = image2, let image3 = image3 {
            
            // 1. Set the image array with UIImage objects
            imageSliderVC.images = [image1, image2, image3]
            
            // 2. If you want, you can set some options
            var options = TNImageSliderViewOptions()
            options.backgroundColor = UIColor.gray
            options.pageControlHidden = true
            options.scrollDirection = .horizontal
            options.pageControlCurrentIndicatorTintColor = UIColor.white
            options.autoSlideIntervalInSeconds = 3
            options.shouldStartFromBeginning = true
            options.imageContentMode = .scaleAspectFill
            imageSliderVC.options = options
        } else {
            print("[ViewController] Could not find one of the images in the image catalog")
        }
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //topMostController()
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
        initialPositionRecentsContainer()
        if  screenHeight == 568 {
            constantTopUp = 0
            constantTopDown = 650
        }
        else if screenHeight == 667 {
            constantTopUp = 0
            constantTopDown = 720
        }
        else if screenHeight == 736 {
            constantTopUp = 0
            constantTopDown = 790
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 480 {
            departmentsLabel.isHidden = true
            housesLabel.isHidden = true
            terrainsLabel.isHidden = true
            houseIcon.isHidden = true
            terrainIcon.isHidden = true
            deptoIcon.isHidden = true
        }
        else if screenHeight == 568 {
            heightConstraint.constant = 569
        }
        else if screenHeight == 667 {
            heightConstraint.constant = 668
            widthConstraint.constant = 380
            heigthSearchButtonConstraint.constant = 70
            widthSearchButtonConstraint.constant = 70
            widthRecentIconConstraint.constant = 24
            heightRecentIconConstraint.constant = 24
            leadingRecentIconConstraint.constant = 100
            recentsLabel.font = recentsLabel.font.withSize(20)
            headerLabel1.font = headerLabel1.font.withSize(21)
            headerLabel2.font = headerLabel2.font.withSize(25)
            departmentsLabel.font = departmentsLabel.font.withSize(15)
            housesLabel.font = housesLabel.font.withSize(15)
            terrainsLabel.font = terrainsLabel.font.withSize(15)
        }
        else if screenHeight == 736 {
            heightConstraint.constant = 737
            widthConstraint.constant = 414
            heigthSearchButtonConstraint.constant = 80
            widthSearchButtonConstraint.constant = 80
            widthRecentIconConstraint.constant = 26
            heightRecentIconConstraint.constant = 26
            leadingRecentIconConstraint.constant = 110
            recentsLabel.font = recentsLabel.font.withSize(22)
            headerLabel1.font = headerLabel1.font.withSize(22)
            headerLabel2.font = headerLabel2.font.withSize(26)
            departmentsLabel.font = departmentsLabel.font.withSize(16)
            housesLabel.font = housesLabel.font.withSize(16)
            terrainsLabel.font = terrainsLabel.font.withSize(16)
        }
    }
    
    func initialPositionRecentsContainer() {
        newConstraint = NSLayoutConstraint(item: recentsContainerView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: constantTopDown)
        self.view.removeConstraint(topConstraint)
        self.view.addConstraint(newConstraint)
        self.view.layoutIfNeeded()
        topConstraint = newConstraint
        if returnFromProductDetail == true {
            showRecentsContainer()
        }
    }

    @IBAction func recentsButton(_ sender: UIButton) {
        showRecentsContainer()
    }
    
    func showRecentsContainer() {
        if(recentsContainerView.isHidden == true){
            recentsContainerView.isHidden = false
            newConstraint = NSLayoutConstraint(item: recentsContainerView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: constantTopUp)
            UIView.animate(withDuration: 0.6, delay: 0.0, options: .curveEaseOut , animations: {
                self.view.removeConstraint(self.topConstraint)
                self.view.addConstraint(self.newConstraint)
                self.view.layoutIfNeeded()
                }, completion: nil)
            topConstraint = newConstraint
        }
        else {
            newConstraint = NSLayoutConstraint(item: recentsContainerView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: constantTopDown)
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut , animations: {
                self.view.removeConstraint(self.topConstraint)
                self.view.addConstraint(self.newConstraint)
                self.view.layoutIfNeeded()
                }, completion: nil)
            topConstraint = newConstraint
            delay(0.4){
                self.recentsContainerView.isHidden = true
            }
        }

    }
    

}//END VIEW CONTROLLER

