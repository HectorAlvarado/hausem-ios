//
//  recentsViewController.swift
//  Hausem
//
//  Created by Hector on 21/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class recentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Header
    @IBOutlet weak var recentsBackButton: UIButton!
    @IBOutlet weak var recentsLabel: UILabel!
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var heightConstraintRecentsIcon: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintRecentsIcon: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintRecentsIcon: NSLayoutConstraint!
    
    //Recent's Table
    @IBOutlet weak var recentsTable: UITableView!
    var storedOffsets = [Int: CGFloat]()
    var productsType = ["CASAS", "DEPARTAMENTOS", "TERRENOS"]
    var productsIcon = [UIImage(named: "house_icon_outline_dark"), UIImage(named: "depto_icon_outline_dark"), UIImage(named: "terrain_icon_outline_dark")]
    var productTypeSelected = 0
    var productIdSelected = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //topMostController()
    }
    
    @IBAction func recentsBackButton(_ sender: UIButton) {
        startController.showRecentsContainer()
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 667 {
            recentsLabel.font = recentsLabel.font.withSize(20)
            heightConstraintRecentsIcon.constant = 24
            widthConstraintRecentsIcon.constant = 24
            leadingConstraintRecentsIcon.constant = 115
        }
        else if screenHeight == 736 {
            recentsLabel.font = recentsLabel.font.withSize(22)
            heightConstraintRecentsIcon.constant = 26
            widthConstraintRecentsIcon.constant = 26
            leadingConstraintRecentsIcon.constant = 125
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.recentsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
        cell.productLabel.text = productsType[indexPath.row]
        cell.productIcon.image = productsIcon[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? CustomCell else { return }
        
        tableViewCell.prepareCollectionView(self, self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? CustomCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension recentsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return productsNameHouses.count
        } else if collectionView.tag == 1 {
            return productsNameDeptos.count
        } else {
            return productsNameTerrains.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! collectionCell
        
        if collectionView.tag == 0 { // HOUSES
            let url = URL(string: productsImagesHouses[indexPath.row])
            cell.productImage.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: {(Bool)  in
            } )
            
            cell.productName.text = productsNameHouses[indexPath.row]
            cell.productAddress.text = productsAddressHouses[indexPath.row]
            cell.bathroomIcon.image = productsBathroomIconHouses[indexPath.row]
            cell.bedroomIcon.image = productsBedroomIconHouses[indexPath.row]
            cell.saleRentLabel.text = productsSaleRentHouses[indexPath.row]
            
            let nfPrice = NumberFormatter()
            nfPrice.numberStyle = .currency
            let sPrice = nfPrice.string(from: NSNumber(value: productsPriceHouses[indexPath.row]))
            cell.productPrice.text = sPrice
            
            let nfBathroom = NumberFormatter()
            nfBathroom.numberStyle = .decimal
            let sBathroom = nfBathroom.string(from: productsBathroomHouses[indexPath.row])
            cell.bathroomLabel.text = sBathroom
            
            let nfBedroom = NumberFormatter()
            nfBedroom.numberStyle = .decimal
            let sBedroom = nfBedroom.string(from: productsBedroomHouses[indexPath.row])
            cell.bedroomLabel.text = sBedroom
        }
        if collectionView.tag == 1 { // DEPTOS
            let url = URL(string: productsImagesDeptos[indexPath.row])
            cell.productImage.af_setImage(withURL:url!, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: {(Bool)  in
            } )
            
            cell.productName.text = productsNameDeptos[indexPath.row]
            cell.productAddress.text = productsAddressDeptos[indexPath.row]
            cell.bathroomIcon.image = productsBathroomIconDeptos[indexPath.row]
            cell.bedroomIcon.image = productsBedroomIconDeptos[indexPath.row]
            cell.saleRentLabel.text = productsSaleRentDeptos[indexPath.row]
            
            let nfPrice = NumberFormatter()
            nfPrice.numberStyle = .currency
            let sPrice = nfPrice.string(from: NSNumber(value: productsPriceDeptos[indexPath.row]))
            cell.productPrice.text = sPrice
            
            let nfBathroom = NumberFormatter()
            nfBathroom.numberStyle = .decimal
            let sBathroom = nfBathroom.string(from: productsBathroomDeptos[indexPath.row])
            cell.bathroomLabel.text = sBathroom
            
            let nfBedroom = NumberFormatter()
            nfBedroom.numberStyle = .decimal
            let sBedroom = nfBedroom.string(from: productsBedroomDeptos[indexPath.row])
            cell.bedroomLabel.text = sBedroom
        }
        if collectionView.tag == 2 { // TERRAINS
            let url = URL(string: productsImagesTerrains[indexPath.row])
            cell.productImage.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: {(Bool)  in
            })
            
            cell.productName.text = productsNameTerrains[indexPath.row]
            cell.productAddress.text = productsAddressTerrains[indexPath.row]
            cell.bathroomIcon.isHidden = true
            cell.bedroomIcon.isHidden = true
            cell.saleRentLabel.text = productsSaleRentTerrains[indexPath.row]
            
            let nfPrice = NumberFormatter()
            nfPrice.numberStyle = .currency
            let sPrice = nfPrice.string(from: NSNumber(value: productsPriceTerrains[indexPath.row]))
            cell.productPrice.text = sPrice
            cell.bathroomLabel.isHidden = true
            cell.bedroomLabel.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath.row)")
        productTypeSelected = collectionView.tag
        productIdSelected = indexPath.row
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToProductDetailFromRecents", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToProductDetailFromRecents") {
            print("Enter to segue goToProductsDetail")
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! productDetailsViewController
            // your new view controller should have property that will store passed value
            viewController.whereItComesFrom = "recentsProducts"
            viewController.productType = productTypeSelected
            viewController.idProduct = productIdSelected
        }
    }
}
