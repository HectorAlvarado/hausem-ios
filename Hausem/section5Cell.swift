//
//  section5Cell.swift
//  Hausem
//
//  Created by Hector on 05/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class section5Cell: UITableViewCell {
    
    @IBOutlet weak var titleSection5: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var heightConstraintButtons: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintButtons: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintIcon: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintIcon: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraintIcon: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            heightConstraintButtons.constant = 50
            widthConstraintButtons.constant = 175
            heightConstraintIcon.constant = 38
            widthConstraintIcon.constant = 38
            leadingConstraintIcon.constant = 100
            titleSection5.font = titleSection5.font.withSize(15)
            locationLabel.font = locationLabel.font.withSize(15)
        }
        else if screenHeight == 736 {
            heightConstraintButtons.constant = 60
            widthConstraintButtons.constant = 210
            heightConstraintIcon.constant = 45
            widthConstraintIcon.constant = 45
            leadingConstraintIcon.constant = 108
            titleSection5.font = titleSection5.font.withSize(16)
            locationLabel.font = locationLabel.font.withSize(16)
        }
    }

}
