//
//  preloaderViewController.swift
//  Hausem
//
//  Created by Hector on 15/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class preloaderViewController: UIViewController {
    
    @IBOutlet weak var preloaderView: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         getTop10Products()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func screenIphone4() -> Bool {
        var iphone4Selected = false
        if screenHeight == 480 {
            iphone4Selected = true
        } else {
            iphone4Selected = false
        }
        return iphone4Selected
    }
    
    func getTop10Products() {
        preloaderView.startAnimating()
        request("http://hausem.com/api/products/top_5", method: .post, parameters: ["foo": "bar"], encoding: JSONEncoding.default)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let JSON = response.result.value {
                        top10Dictionary = JSON as! NSDictionary
                        top10Houses = top10Dictionary.object(forKey: "house_properties") as! NSArray
                        top10Deptos = top10Dictionary.object(forKey: "department_properties") as! NSArray
                        top10Terrains = top10Dictionary.object(forKey: "terrain_properties") as! NSArray
                        self.loadHouses()
                    }
                    print("Validation Successful")
                case .failure(let error):
                    print(error)
                    self.preloaderView.stopAnimating()
                    _ = SweetAlert().showAlert("Error en servidor!", subTitle: "No se pudo establecer una conexión con el servidor.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Intentar nuevamente", buttonColor:UIColor.colorFromRGB(0x11B1A8)) { (isOtherButton) -> Void in
                        if isOtherButton == true {
                            self.getTop10Products()
                        }
                    }
                }
            }
    }
    
    
    func loadHouses() {
        for product in (top10Houses as? [[String:Any]])! {
            if let image = product["product_main_pic"] as? String {
                let imageEncoding = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                productsImagesHouses.append(imageEncoding)
            }
            if let name = product["product_title"] as? String {
                productsNameHouses.append(name)
                productsBathroomIconHouses.append(UIImage(named: "bathroom_icon_outline")!)
                productsBedroomIconHouses.append(UIImage(named: "bedroom_icon_outline")!)
            }
            if let price = product["product_price"] as? Float {
                productsPriceHouses.append(price)
            }
            if let address = product["product_state"] as? String {
                productsAddressHouses.append(address)
            }
            if let sale = product["product_sale"] as? Bool , let rent = product["product_rent"] as? Bool {
                let rv = sale == true && rent == true ? "RENTA/VENTA" : sale == true && rent == false ? "VENTA" : sale == false && rent == true ? "RENTA" : "S/D"
                    productsSaleRentHouses.append(rv)
            }
            if let amenities = product["_amenities"] as? NSDictionary {
                let bathrooms = amenities.object(forKey: "bathroom") as! NSNumber
                let bedrooms = amenities.object(forKey: "room_number") as! NSNumber
                productsBathroomHouses.append(bathrooms)
                productsBedroomHouses.append(bedrooms)
            }
        }
        loadDeptos()
    }
    
    func loadDeptos() {
        for product in (top10Deptos as? [[String:Any]])! {
            if let image = product["product_main_pic"] as? String {
                let imageEncoding = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                productsImagesDeptos.append(imageEncoding)
            }
            if let name = product["product_title"] as? String {
                productsNameDeptos.append(name)
                productsBathroomIconDeptos.append(UIImage(named: "bathroom_icon_outline")!)
                productsBedroomIconDeptos.append(UIImage(named: "bedroom_icon_outline")!)
            }
            if let price = product["product_price"] as? Float {
                productsPriceDeptos.append(price)
            }
            if let address = product["product_state"] as? String {
                productsAddressDeptos.append(address)
            }
            if let sale = product["product_sale"] as? Bool , let rent = product["product_rent"] as? Bool {
                let rv = sale == true && rent == true ? "RENTA/VENTA" : sale == true && rent == false ? "VENTA" : sale == false && rent == true ? "RENTA" : "S/D"
                productsSaleRentDeptos.append(rv)
            }
            if let amenities = product["_amenities"] as? NSDictionary {
                let bathrooms = amenities.object(forKey: "bathroom") as! NSNumber
                let bedrooms = amenities.object(forKey: "room_number") as! NSNumber
                productsBathroomDeptos.append(bathrooms)
                productsBedroomDeptos.append(bedrooms)
            }
        }
        loadTerrains()
    }
    
    func loadTerrains() {
        for product in (top10Terrains as? [[String:Any]])! {
            if let image = product["product_main_pic"] as? String {
                let imageEncoding = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                productsImagesTerrains.append(imageEncoding)
            }
            if let name = product["product_title"] as? String {
                productsNameTerrains.append(name)
                productsBathroomIconTerrains.append(UIImage(named: "bathroom_icon_outline")!)
                productsBedroomIconTerrains.append(UIImage(named: "bedroom_icon_outline")!)
            }
            if let price = product["product_price"] as? Float {
                productsPriceTerrains.append(price)
            }
            if let address = product["product_state"] as? String {
                productsAddressTerrains.append(address)
            }
            if let sale = product["product_sale"] as? Bool , let rent = product["product_rent"] as? Bool {
                let rv = sale == true && rent == true ? "RENTA/VENTA" : sale == true && rent == false ? "VENTA" : sale == false && rent == true ? "RENTA" : "S/D"
                productsSaleRentTerrains.append(rv)
            }
            if let amenities = product["_amenities"] as? NSDictionary {
                if let bathrooms = amenities.object(forKey: "bathroom") as? NSNumber {
                    productsBathroomTerrains.append(bathrooms)
                } else {
                    productsBathroomTerrains.append(0)
                }
                if let bedrooms = amenities.object(forKey: "room_number") as? NSNumber {
                    productsBedroomTerrains.append(bedrooms)
                } else {
                    productsBedroomTerrains.append(0)
                }
            }
        }
        
        self.preloaderView.stopAnimating()
        self.performSegue(withIdentifier: "goToHome", sender: self)
    }


}//END VIEW CONTROLLER
