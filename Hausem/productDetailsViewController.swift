//
//  productDetailsViewController.swift
//  Hausem
//
//  Created by Hector on 28/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class productDetailsViewController: UIViewController, UITextFieldDelegate {
    
    var imageSliderVC:TNImageSliderViewControllerFromUrl!
    @IBOutlet weak var prductImagesContainerView: UIView!
    var image1_URL:UIImageView!
    
    //Header
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var saleRentLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var imageUrlArray = [String]()
    /* Body */
    var realEstateInfo: NSDictionary!
    var objectProduct: NSDictionary!
    var productRealEstateToSend = "", productNameToSend = "", productAddressToSend = ""
    
    //Passed values
    var whereItComesFrom = "recentsProducts"
    var productType = 0
    var idProduct = 0
    var idRealEstate = 0
    
    //View1
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAddress: UILabel!
    
    //View2
    @IBOutlet weak var propertyAreaLabel: UILabel!
    @IBOutlet weak var propertyConstructionLabel: UILabel!
    @IBOutlet weak var kitchenTypeLabel: UILabel!
    @IBOutlet weak var landUseLabel: UILabel!
    @IBOutlet weak var propertyAreaLabel2: UILabel!
    @IBOutlet weak var propertyConstructionLabel2: UILabel!
    @IBOutlet weak var kitchenTypeLabel2: UILabel!
    @IBOutlet weak var landUseLabel2: UILabel!
    
    //View 3
    @IBOutlet weak var bathroomLabel: UILabel!
    @IBOutlet weak var bedroomLabel: UILabel!
    @IBOutlet weak var bathroomIcon: UIImageView!
    @IBOutlet weak var bedroomIcon: UIImageView!
    
    //View 4
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amenitiesLabel: UILabel!
    var urlParameters = "?"
    
    //View5
    @IBOutlet weak var descriptionProductTextView: UITextView!
    /* END Body */
    
    //Amenities
    @IBOutlet weak var viewAmenities: UIView!
    @IBOutlet weak var viewAmenitiesDetails: UIView!
    @IBOutlet weak var amenitiesButton: UIButton!
    @IBOutlet weak var acceptAmenitiesButton: UIButton!
    @IBOutlet weak var amenitiesHeaderLabel: UILabel!
    @IBOutlet weak var amenitiesTextView: UITextView!
    
    //Dates
    @IBOutlet weak var viewDates: UIView!
    @IBOutlet weak var viewDatesDetails: UIView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var acceptDateButton: UIButton!
    @IBOutlet weak var cancelDateButton: UIButton!
    @IBOutlet weak var clientName: JiroTextField!
    @IBOutlet weak var clientEmail: JiroTextField!
    @IBOutlet weak var clientPhone: JiroTextField!
    @IBOutlet weak var dateHeaderLabel: UILabel!
    
    //Footer's constraints
    @IBOutlet weak var widthAmenitiesButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightAmenitiesButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingAmenitiesButtonConstraint: NSLayoutConstraint!
    
    //Keyboard's config
    var originY: CGFloat! = 0
    var newY: CGFloat! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(productDetailsViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(productDetailsViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(productDetailsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        originY = self.view.frame.origin.y
        clientName.delegate = self
        clientEmail.delegate = self
        clientPhone.delegate = self
        checkTypeProduct()
        imageSilder()
        productNameToSend = productName.text!
        productAddressToSend = productAddress.text!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //topMostController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dismissKeyboard()
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewAmenities.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        viewDates.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("[ViewController] Prepare for segue")
        if( segue.identifier == "seg_imageSlider2" ) {
            imageSliderVC = segue.destination as! TNImageSliderViewControllerFromUrl
        }
        if( segue.identifier == "goToRecentsFromProductDetails" ) {
            let viewController = segue.destination as! ViewController
            viewController.returnFromProductDetail = true
        }
        if( segue.identifier == "goToRealEstateProducts2" ) {
            let viewController = segue.destination as! realEstateProductsViewController
            viewController.idRealEstate = idRealEstate
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func keyboardWillShow(_ notification: Notification) {
        if originY == self.view.frame.origin.y {
            self.view.frame.origin.y -= newY
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.view.frame.origin.y = originY
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func amenitiesButton(_ sender: UIButton) {
        self.viewAmenities.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.viewAmenities.fadeIn()
        })
        viewAmenities.isHidden = false
    }

    @IBAction func acceptAmenitiesButton(_ sender: UIButton) {
        self.viewAmenities.fadeOutCalls(completion: {
            (finished: Bool) -> Void in
            self.viewAmenities.fadeIn()
        })
        delay(0.6){
            self.viewAmenities.isHidden = true
        }
    }
    
    @IBAction func dateButton(_ sender: UIButton) {
        self.viewDates.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.viewDates.fadeIn()
        })
        viewDates.isHidden = false
    }
    
    @IBAction func acceptCancelDateButton(_ sender: UIButton) {
        dismissKeyboard()
        switch sender {
        case acceptDateButton:
            sendEmailToRealEstate()
        case cancelDateButton:
            self.viewDates.fadeOutCalls(completion: {
                (finished: Bool) -> Void in
                self.viewDates.fadeIn()
            })
            delay(0.6){
                self.viewDates.isHidden = true
            }
        default:
            break
        }
    }
    
    func sendEmailToRealEstate() {
        if (clientName.text?.isEmpty)! || (clientPhone.text?.isEmpty)! || (clientEmail.text?.isEmpty)! {
            _ = SweetAlert().showAlert("Datos inválidos!", subTitle: "Los datos son inválidos.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
        } else {
            urlParameters += "id_real_estate=\(productRealEstateToSend)&client_name=\(clientName.text!)&client_phone=\(clientPhone.text!)&client_email=\(clientEmail.text!)&product_name=\(productNameToSend)&product_address=\(productAddressToSend)"
            let urlParametersEncoding = urlParameters.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            print("ESTA ES LA URL QUE MANDA---> \(urlParametersEncoding)")
            request("http://hausem.com/api/send_email\(urlParametersEncoding)", method: .post, encoding: URLEncoding.default)
                .validate()
                .responseString { response in
                    switch response.result {
                    case .success:
                        if let response = response.result.value {
                            if response == "YES" {
                                self.urlParameters = "?"
                                _ = SweetAlert().showAlert("Solicitud enviada!", subTitle: "La inmobiliaria se pondrá en contacto.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8)) { (isOtherButton) -> Void in
                                    if isOtherButton == true {
                                        self.viewDates.fadeOutCalls(completion: {
                                            (finished: Bool) -> Void in
                                            self.viewDates.fadeIn()
                                        })
                                        delay(0.6){
                                            self.viewDates.isHidden = true
                                        }
                                    }
                                }
                            } else {
                               self.urlParameters = "?"
                               _ = SweetAlert().showAlert("Solicitud NO enviada!", subTitle: "No se estableció una conexión con el servidor.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
                            }
                        }
                        print("Validation Successful")
                    case .failure(let error):
                        print(error)
                        _ = SweetAlert().showAlert("Solicitud NO enviada!", subTitle: "No se estableció una conexión con el servidor.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8))
                    }
            }
        }
    }
    
    func imageSilder() {
        let imageUrlArrayFiltered = imageUrlArray.filter { $0 != "" }
        imageSliderVC.images = imageUrlArrayFiltered
        var options = TNImageSliderViewOptionsFromUrl()
        options.backgroundColor = UIColor.gray
        options.pageControlHidden = false
        options.scrollDirection = .horizontal
        options.pageControlCurrentIndicatorTintColor = UIColor.white
        options.autoSlideIntervalInSeconds = 6
        options.shouldStartFromBeginning = true
        options.imageContentMode = .scaleAspectFill
        imageSliderVC.options = options
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 568 {
            newY = 90
            saleRentLabel.font = saleRentLabel.font.withSize(18)
            priceLabel.font = priceLabel.font.withSize(18)
        }
        else if screenHeight == 667 {
            newY = 110
            
            //Header
            saleRentLabel.font = saleRentLabel.font.withSize(20)
            priceLabel.font = priceLabel.font.withSize(20)
            
            //View 1
            productName.font = productName.font.withSize(19)
            productAddress.font = productAddress.font.withSize(15)
            
            //View 2
            propertyAreaLabel.font = propertyAreaLabel.font.withSize(17)
            propertyAreaLabel2.font = propertyAreaLabel2.font.withSize(16)
            propertyConstructionLabel.font = propertyConstructionLabel.font.withSize(17)
            propertyConstructionLabel2.font = propertyConstructionLabel2.font.withSize(16)
            kitchenTypeLabel.font = kitchenTypeLabel.font.withSize(17)
            kitchenTypeLabel2.font = kitchenTypeLabel2.font.withSize(16)
            landUseLabel.font = landUseLabel.font.withSize(17)
            landUseLabel2.font = landUseLabel2.font.withSize(16)
            
            descriptionProductTextView.font = descriptionProductTextView.font!.withSize(15)
            bathroomLabel.font = bathroomLabel.font.withSize(17)
            bedroomLabel.font = bedroomLabel.font.withSize(17)
            dateLabel.font = dateLabel.font.withSize(16)
            amenitiesLabel.font = amenitiesLabel.font.withSize(16)
            
            amenitiesHeaderLabel.font = amenitiesHeaderLabel.font.withSize(17)
            amenitiesTextView.font = amenitiesTextView.font!.withSize(17)
            acceptAmenitiesButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            
            dateHeaderLabel.font = dateHeaderLabel.font.withSize(17)
            acceptDateButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            cancelDateButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            
            widthAmenitiesButtonConstraint.constant = 150
            heightAmenitiesButtonConstraint.constant = 50
            leadingAmenitiesButtonConstraint.constant = 25
        }
        else if screenHeight == 736 {
            newY = 110
            
            //Header
            saleRentLabel.font = saleRentLabel.font.withSize(22)
            priceLabel.font = priceLabel.font.withSize(22)
            
            //View 1
            productName.font = productName.font.withSize(20)
            productAddress.font = productAddress.font.withSize(16)
            
            //View 2
            propertyAreaLabel.font = propertyAreaLabel.font.withSize(18)
            propertyAreaLabel2.font = propertyAreaLabel2.font.withSize(17)
            propertyConstructionLabel.font = propertyConstructionLabel.font.withSize(18)
            propertyConstructionLabel2.font = propertyConstructionLabel2.font.withSize(17)
            kitchenTypeLabel.font = kitchenTypeLabel.font.withSize(18)
            kitchenTypeLabel2.font = kitchenTypeLabel2.font.withSize(17)
            landUseLabel.font = landUseLabel.font.withSize(18)
            landUseLabel2.font = landUseLabel2.font.withSize(17)
            
            descriptionProductTextView.font = descriptionProductTextView.font!.withSize(16)
            bathroomLabel.font = bathroomLabel.font.withSize(18)
            bedroomLabel.font = bedroomLabel.font.withSize(18)
            dateLabel.font = dateLabel.font.withSize(17)
            amenitiesLabel.font = amenitiesLabel.font.withSize(17)
            
            amenitiesHeaderLabel.font = amenitiesHeaderLabel.font.withSize(19)
            amenitiesTextView.font = amenitiesTextView.font!.withSize(19)
            acceptAmenitiesButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            
            dateHeaderLabel.font = dateHeaderLabel.font.withSize(19)
            acceptDateButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            cancelDateButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            
            widthAmenitiesButtonConstraint.constant = 170
            heightAmenitiesButtonConstraint.constant = 50
            leadingAmenitiesButtonConstraint.constant = 25
        }
    }
    
    func checkTypeProduct() {
        if productType == 0 {//COMES FROM RECENTS HOUSES
            objectProduct = top10Houses[idProduct] as! NSDictionary
        } else if productType == 1 {//COMES FROM RECENTS DEPTOS
            objectProduct = top10Deptos[idProduct] as! NSDictionary
        } else if productType == 2 {//COMES FROM RECENTS TERRAINS
            objectProduct = top10Terrains[idProduct] as! NSDictionary
        }
        else {//COMES FROM REAL ESTATES PRODUCTS
            realEstateInfo = searchResults[idRealEstate] as? NSDictionary
            let productsInfo = realEstateInfo["_products"] as? NSArray
            objectProduct = productsInfo![idProduct] as! NSDictionary
        }
        loadInfo()
    }
    
    func loadInfo() {
        productRealEstateToSend = objectProduct["product_real_estate"]! as! String
        imageUrlArray = [objectProduct["product_main_pic"]! as! String, objectProduct["product_pic_one"] as! String, objectProduct["product_pic_two"] as! String, objectProduct["product_pic_three"] as! String, objectProduct["product_pic_four"] as! String, objectProduct["product_pic_five"] as! String]
        let price = objectProduct["product_price"] as? Float
        let nfPrice = NumberFormatter(); nfPrice.numberStyle = .currency
        let sPrice = nfPrice.string(from: NSNumber(value: price!))
        priceLabel.text = sPrice
        productName.text = objectProduct["product_title"] as? String
        productAddress.text = objectProduct["product_address"] as? String
        descriptionProductTextView.text = objectProduct["product_description"] as? String
        let sale = objectProduct["product_sale"] as? Bool
        let rent = objectProduct["product_rent"] as? Bool
        saleRentLabel.text = sale == true && rent == true ? "R/V" : sale == true && rent == false ? "VENTA" : sale == false && rent == true ? "RENTA" : "S/D"
        
        if let landArea = objectProduct["product_land_area"] as? NSNumber {
            propertyAreaLabel2.text = "\(landArea.stringValue) m2"
        } else {
            propertyAreaLabel2.text = "Sin especificar"
        }
        if let constructionArea = objectProduct["product_construction_area"] as? NSNumber {
            propertyConstructionLabel2.text = "\(constructionArea.stringValue) m2"
        } else {
            propertyConstructionLabel2.text = "Sin especificar"
        }
        if let amenities = objectProduct["_amenities"] as? NSDictionary {
            let kitchen = amenities.object(forKey: "kitchen") as? String
            let landUse = amenities.object(forKey: "land_use") as? String
            if kitchen == "" {
                kitchenTypeLabel2.text = "Sin especificar"
            } else {
                kitchenTypeLabel2.text = kitchen
            }
            if landUse == "" {
                landUseLabel2.text = "Sin especificar"
            } else {
                landUseLabel2.text = landUse
            }
            if let bathrooms = amenities.object(forKey: "bathroom") as? NSNumber {
                bathroomLabel.text = bathrooms.stringValue
            } else {
                bathroomLabel.isHidden = true
                bathroomIcon.isHidden = true
            }
            if let bedrooms = amenities.object(forKey: "room_number") as? NSNumber {
               bedroomLabel.text = bedrooms.stringValue
            } else {
                bedroomLabel.isHidden = true
                bedroomIcon.isHidden = true
            }
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "pool") as! Bool) == true ? "Alberca\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "a_c") as! Bool) == true ? "Aire acondicionado\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "cable_service") as! Bool) == true ? "Cable\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "heating") as! Bool) == true ? "Calefacción\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "chimeney") as! Bool) == true ? "Chimenea\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "laundry_room") as! Bool) == true ? "Cuarto de lavandería\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "service_room") as! Bool) == true ? "Cuarto de servicio\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "elevator") as! Bool) == true ? "Elevador\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "parking") as! Bool) == true ? "Estacionamiento\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "garage") as! Bool) == true ? "Cochera\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "natural_gas") as! Bool) == true ? "Gas natural\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "internet_service") as! Bool) == true ? "Internet\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "garden") as! Bool) == true ? "Jardín\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "security") as! Bool) == true ? "Seguridad\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "electricity_service") as! Bool) == true ? "Servicio eléctrico\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "phone_service") as! Bool) == true ? "Teléfono\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "terrace") as! Bool) == true ? "Terraza\n" : "")
            amenitiesTextView.text = amenitiesTextView.text + ((amenities.object(forKey: "dressing_room") as! Bool) == true ? "Vestidor\n" : "")
        }
    }
    
    @IBAction func goBackButton(_ sender: UIButton) {
        var whereToGo = ""
        whereToGo = whereItComesFrom == "realEstateProducts" ? "goToRealEstateProducts2" : "goToRecentsFromProductDetails"
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: whereToGo, sender: self)
        }
    }


}// END OS VIEW CONTROLLER
