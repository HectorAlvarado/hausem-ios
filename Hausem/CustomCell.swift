//
//  CustomCell.swift
//  Hausem
//
//  Created by Hector on 22/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            productLabel.font = productLabel.font.withSize(21)
        }
        else if screenHeight == 736 {
            productLabel.font = productLabel.font.withSize(22)
        }
    }
}

extension CustomCell {
    
   func prepareCollectionView(_ delegate: UICollectionViewDelegate, _ dataSource: UICollectionViewDataSource, forRow row: Int) {
    
        collectionView.delegate = delegate
        collectionView.dataSource = dataSource
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set {
            collectionView.contentOffset.x = newValue
        }
        
        get {
            return collectionView.contentOffset.x
        }
    }
}

