//
//  searchViewController.swift
//  Hausem
//
//  Created by Hector on 26/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class searchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    //Header
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var aplyButton: UIButton!
    
    //Loading View
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var preloaderIndicator: UIActivityIndicatorView!
    
    //Body
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var selectionView: UIView!
    var textFieldArray = [String]()
    var  urlParameters = "?", rentChecked = false, saleChecked = false , houseChecked = false , deptoChecked = false, terrainChecked = false, bathroomChecked = 0, bedroomChecked = 0, locationChecked = 0, heating = false, chimney = false, internet = false, phone = false, balcony = false, garden = false, tv = false, pool = false, security = false, ac = false, garage = false, serviceRoom = false, laundry = false, electricity = false, elevator = false, naturalGas = false, dressingRoom = false, parking = false, minPrice: Double! = 0, maxPrice: Double! = 0, priceChecked = 0
    
    //Bathrooms View
    var lastSelectedIndexPathBathroom = 0
    var bathroomSelected = "Elegir"
    @IBOutlet weak var bathroomsView: UIView!
    @IBOutlet weak var bathroomsTable: UITableView!
    @IBOutlet weak var titleBathroomsLabel: UILabel!
    @IBOutlet weak var acceptBathroomsButton: UIButton!
    var bathroomsNumber = ["Sin especificar","1 baño","2 baños","3 baños","4 baños","5 baños","6 baños","7 baños","8 baños","9 baños","10 baños"]
    
    //Bedrooms View
    var lastSelectedIndexPathBedroom = 0
    var bedroomSelected = "Elegir"
    @IBOutlet weak var bedroomsView: UIView!
    @IBOutlet weak var bedroomsTable: UITableView!
    @IBOutlet weak var titleBedroomsLabel: UILabel!
    @IBOutlet weak var acceptBedroomsButton: UIButton!
    var bedroomsNumber = ["Sin especificar", "1 cuarto", "2 cuartos", "3 cuartos", "4 cuartos", "5 cuartos", "6 cuartos", "7 cuartos", "8 cuartos", "9 cuartos", "10 cuartos"]
    
    //Location View
    var lastSelectedIndexPathLocation = 0
    var locationSelected = "Elegir"
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTable: UITableView!
    @IBOutlet weak var titleLocationLabel: UILabel!
    @IBOutlet weak var acceptLocationButton: UIButton!
    var locationsNumber = ["Todas las ubicaciones","Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Coahuila", "Colima", "Chiapas", "Chihuahua", "Distrito Federal", "Durango", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "México", "Michoacán", "Morelos", "Nayarit", "Nuevo León", "Oaxaca", "Puebla", "Querétaro", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"]
    
    //Prices View
    var lastSelectedIndexPathPrice = 0
    var priceSelected = "Sin especificar"
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTable: UITableView!
    @IBOutlet weak var titlePriceLabel: UILabel!
    @IBOutlet weak var acceptPriceButton: UIButton!
    var priceNumber = ["Sin especificar", "$0 - $500,000.00", "$500,001.00 - $1,000,000.00", "$1,000,001.00 - $2,000,000.00", "$2,000,001.00 - $3,000,000.00", "$3,000,001.00 - $4,000,000.00", "$4,000,001.00 - $5,000,000.00", "5,000,001.00 - $6,000,000.00", "$6,000,001.00 - $7,000,000.00", "$7,000,001.00 - $8,000,000.00", "Mayor a $8,000,001.00"]
    
    //Footer
    @IBOutlet weak var deleteFiltersLabel: UILabel!
    
    //Keyboard's config
    var originY: CGFloat! = 0
    var newY: CGFloat! = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        resetSearchResults()
        originY = self.view.frame.origin.y
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
        //topMostController()
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        selectionView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        loadingView.backgroundColor = UIColor(red: CGFloat(17/255.0), green: CGFloat(177/255.0), blue: CGFloat(168/255.0), alpha: 0.6)
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 568 {
            aplyButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 18)
            deleteFiltersLabel.font = deleteFiltersLabel.font.withSize(16)
        }
        else if screenHeight == 667 {
            aplyButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 20)
            deleteFiltersLabel.font = deleteFiltersLabel.font.withSize(18)
            titleBathroomsLabel.font = titleBathroomsLabel.font.withSize(17)
            acceptBathroomsButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            titleBedroomsLabel.font = titleBedroomsLabel.font.withSize(17)
            acceptBedroomsButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
            titleLocationLabel.font = titleLocationLabel.font.withSize(17)
            acceptLocationButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 17)
        }
        else if screenHeight == 736 {
            aplyButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 22)
            deleteFiltersLabel.font = deleteFiltersLabel.font.withSize(20)
            titleBathroomsLabel.font = titleBathroomsLabel.font.withSize(19)
            acceptBathroomsButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            titleBedroomsLabel.font = titleBedroomsLabel.font.withSize(19)
            acceptBedroomsButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
            titleLocationLabel.font = titleLocationLabel.font.withSize(19)
            acceptLocationButton.titleLabel!.font = UIFont(name: "Roboto-Regular", size: 19)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberRows = 0
        if tableView == searchTable {
            numberRows = 7
        }
        if tableView == bathroomsTable {
            numberRows = bathroomsNumber.count
        }
        if tableView == bedroomsTable {
            numberRows = bedroomsNumber.count
        }
        if tableView == locationTable {
            numberRows = locationsNumber.count
        }
        if tableView == priceTable {
            numberRows = priceNumber.count
        }
        return numberRows
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == searchTable {
            if indexPath.row == 0 || indexPath.row == 1 {
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = 170.0
            }
            else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = 130.0
            }
            else if indexPath.row == 5 {
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = 410.0
            }
            else if indexPath.row == 6 {
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = 150.0
            }
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchTable {
            if indexPath.row == 0 {
                /*SECTION 1 ---- RENT OR SALE CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "saleRentCell", for: indexPath) as! section1Cell
                cell.rentButton.tag = 0
                cell.saleButton.tag = 1
                cell.rentButton.isSelected = rentChecked
                cell.saleButton.isSelected = saleChecked
                cell.rentButton.addTarget(self, action: #selector(rentSaleSelection) , for: .touchUpInside)
                cell.saleButton.addTarget(self, action: #selector(rentSaleSelection), for: .touchUpInside)
                /*END SECTION 1 ---- RENT OR SALE CELL*/
                return cell
            }
            else if indexPath.row == 1 {
                /*SECTION 2 ---- HOUSE/DEPARTMENT/TERRAIN CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "typeCell", for: indexPath) as! section2Cell
                cell.houseButton.tag = 0
                cell.departmentButton.tag = 1
                cell.terrainButton.tag = 2
                cell.houseButton.isSelected = houseChecked
                cell.departmentButton.isSelected = deptoChecked
                cell.terrainButton.isSelected = terrainChecked
                cell.houseButton.addTarget(self, action: #selector(typeSelection), for: .touchUpInside)
                cell.departmentButton.addTarget(self, action: #selector(typeSelection), for: .touchUpInside)
                cell.terrainButton.addTarget(self, action: #selector(typeSelection), for: .touchUpInside)
                /*END SECTION 2 ---- HOUSE/DEPARTMENT/TERRAIN CELL*/
                return cell
            }
            else if indexPath.row == 2 {
                /*SECTION 3 ---- BATHROOM CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "bathroomCell", for: indexPath) as! section3Cell
                cell.bathroomButton.tag = indexPath.row
                cell.bathroomButton.addTarget(self, action: #selector(bathroomsSelection(_:)), for: .touchUpInside)
                cell.bathroomLabel.text = bathroomSelected
                /*END SECTION 3 ---- BATHROOM CELL*/
                return cell
                
            }
            else if indexPath.row == 3 {
                /*SECTION 4 ---- BEDROOM CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "bedroomCell", for: indexPath) as! section4Cell
                cell.bedroomButton.tag = indexPath.row
                cell.bedroomButton.addTarget(self, action: #selector(bedroomsSelection(_:)), for: .touchUpInside)
                cell.bedroomLabel.text = bedroomSelected
                /*END SECTION 4 ---- BEDROOM CELL*/
                return cell
            }
            else if indexPath.row == 4 {
                /*SECTION 5 ---- LOCATION CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! section5Cell
                cell.locationButton.tag = indexPath.row
                cell.locationButton.addTarget(self, action: #selector(locationsSelection(_:)), for: .touchUpInside)
                cell.locationLabel.text = locationSelected
                
                /*END SECTION 5 ---- LOCATION CELL*/
                return cell
            }
            else if indexPath.row == 5 {
                /*SECTION 6 ---- AMENITY CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "amenitiesCell", for: indexPath) as! section6Cell
                
                cell.amenity1Button.tag = 1; cell.amenity2Button.tag = 2
                cell.amenity3Button.tag = 3; cell.amenity4Button.tag = 4
                cell.amenity5Button.tag = 5; cell.amenity6Button.tag = 6
                cell.amenity7Button.tag = 7; cell.amenity8Button.tag = 8
                cell.amenity9Button.tag = 9; cell.amenity10Button.tag = 10
                cell.amenity11Button.tag = 11; cell.amenity12Button.tag = 12
                cell.amenity13Button.tag = 13; cell.amenity14Button.tag = 14
                cell.amenity15Button.tag = 15; cell.amenity16Button.tag = 16
                cell.amenity17Button.tag = 17; cell.amenity18Button.tag = 18
                
                cell.amenity1Button.isSelected = pool; cell.amenity2Button.isSelected = ac
                cell.amenity3Button.isSelected = tv; cell.amenity4Button.isSelected = heating
                cell.amenity5Button.isSelected = chimney; cell.amenity6Button.isSelected = laundry
                cell.amenity7Button.isSelected = serviceRoom; cell.amenity8Button.isSelected = elevator
                cell.amenity9Button.isSelected = parking; cell.amenity10Button.isSelected = garage
                cell.amenity11Button.isSelected = naturalGas; cell.amenity12Button.isSelected = internet
                cell.amenity13Button.isSelected = garden; cell.amenity14Button.isSelected = security
                cell.amenity15Button.isSelected = electricity; cell.amenity16Button.isSelected = phone
                cell.amenity17Button.isSelected = balcony; cell.amenity18Button.isSelected = dressingRoom
                
                cell.amenity1Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity2Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity3Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity4Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity5Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity6Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity7Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity8Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity9Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity10Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity11Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity12Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity13Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity14Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity15Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity16Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity17Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                cell.amenity18Button.addTarget(self, action: #selector(amenitySelection), for: .touchUpInside)
                
                /*END SECTION 6 ---- AMENITY CELL*/
                return cell
            }
            else {
                /*SECTION 7 ---- PRICE CELL*/
                let cell = self.searchTable.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath) as! section7Cell
                cell.pricelabel2.tag = 1
                cell.priceButton.addTarget(self, action: #selector(pricesSelection(_:)), for: .touchUpInside)
                cell.pricelabel2.text = priceSelected
                /*END SECTION 7 ---- PRICE CELL*/
                return cell
            }
        }
            
        /*BATHROOM SELECT*/
        else if tableView == bathroomsTable {
            let cell = self.bathroomsTable.dequeueReusableCell(withIdentifier: "bathroomsCell", for: indexPath) as! bathroomsCell
            cell.bathroomsLabel.text = bathroomsNumber[indexPath.row]
            if(indexPath.row == lastSelectedIndexPathBathroom) {
                bathroomChecked = indexPath.row
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            return cell
        }/*END BATHROOM SELECT*/
            
        /*BEDROOM SELECT*/
        else if tableView == bedroomsTable {
            let cell = self.bedroomsTable.dequeueReusableCell(withIdentifier: "bedroomsCell", for: indexPath) as! bedroomsCell
            cell.bedroomsLabel.text = bedroomsNumber[indexPath.row]
            if(indexPath.row == lastSelectedIndexPathBedroom) {
                bedroomChecked = indexPath.row
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            return cell
        }/*END BEDROOM SELECT*/
        
        /*LOCATION SELECT*/
        else if tableView == locationTable {
            let cell = self.locationTable.dequeueReusableCell(withIdentifier: "locationsCell", for: indexPath) as! locationCell
            cell.locationLabel.text = locationsNumber[indexPath.row]
            if(indexPath.row == lastSelectedIndexPathLocation) {
                locationChecked = indexPath.row
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            return cell
        }/*END LOCATION SELECT*/
            
        /*PRICES SELECT*/
        else {
            let cell = self.priceTable.dequeueReusableCell(withIdentifier: "pricesCell", for: indexPath) as! pricesCell
            cell.priceLabel.text = priceNumber[indexPath.row]
            if(indexPath.row == lastSelectedIndexPathPrice) {
                priceChecked = indexPath.row
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            return cell
        }/*END PRICES SELECT*/
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == locationTable {
            lastSelectedIndexPathLocation = indexPath.row
            locationSelected = locationsNumber[lastSelectedIndexPathLocation]
            locationTable.reloadData()
        }
        else if tableView == bedroomsTable {
            lastSelectedIndexPathBedroom = indexPath.row
            bedroomSelected = bedroomsNumber[lastSelectedIndexPathBedroom]
            bedroomsTable.reloadData()
        }
        else if tableView == bathroomsTable {
            lastSelectedIndexPathBathroom = indexPath.row
            bathroomSelected = bathroomsNumber[lastSelectedIndexPathBathroom]
            bathroomsTable.reloadData()
        }
        else if tableView == priceTable {
            lastSelectedIndexPathPrice = indexPath.row
            priceSelected = priceNumber[lastSelectedIndexPathPrice]
            priceTable.reloadData()
        }
    }
    
    func rentSaleSelection(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.isSelected == false ? (rentChecked = true) : (rentChecked = false)
        case 1:
            sender.isSelected == false ? (saleChecked = true) : (saleChecked = false)
        default:
            break;
        }
        sender.isSelected == false ? (sender.isSelected = true) : (sender.isSelected = false)
    }
    
    func typeSelection(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.isSelected == false ? (houseChecked = true) : (houseChecked = false)
        case 1:
            sender.isSelected == false ? (deptoChecked = true) : (deptoChecked = false)
        case 2:
            sender.isSelected == false ? (terrainChecked = true) : (terrainChecked = false)
        default:
            break
        }
        sender.isSelected == false ? (sender.isSelected = true) : (sender.isSelected = false)
    }
    
    func bathroomsSelection(_ sender: UIButton) {
        self.selectionView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.selectionView.fadeIn()
        })
        selectionView.isHidden = false
        bathroomsView.isHidden = false
    }
    
    func bedroomsSelection(_ sender: UIButton) {
        self.selectionView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.selectionView.fadeIn()
        })
        selectionView.isHidden = false
        bedroomsView.isHidden = false
    }
    
    func locationsSelection(_ sender: UIButton) {
        self.selectionView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.selectionView.fadeIn()
        })
        selectionView.isHidden = false
        locationView.isHidden = false
    }
    
    func pricesSelection(_ sender: UIButton) {
        self.selectionView.fadeOut(completion: {
            (finished: Bool) -> Void in
            self.selectionView.fadeIn()
        })
        selectionView.isHidden = false
        priceView.isHidden = false
    }
    
    func amenitySelection(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            sender.isSelected == false ? (pool = true) : (pool = false)
        case 2:
            sender.isSelected == false ? (ac = true) : (ac = false)
        case 3:
            sender.isSelected == false ? (tv = true) : (tv = false)
        case 4:
            sender.isSelected == false ? (heating = true) : (heating = false)
        case 5:
            sender.isSelected == false ? (chimney = true) : (chimney = false)
        case 6:
            sender.isSelected == false ? (laundry = true) : (laundry = false)
        case 7:
            sender.isSelected == false ? (serviceRoom = true) : (serviceRoom = false)
        case 8:
            sender.isSelected == false ? (elevator = true) : (elevator = false)
        case 9:
            sender.isSelected == false ? (parking = true) : (parking = false)
        case 10:
            sender.isSelected == false ? (garage = true) : (garage = false)
        case 11:
            sender.isSelected == false ? (naturalGas = true) : (naturalGas = false)
        case 12:
            sender.isSelected == false ? (internet = true) : (internet = false)
        case 13:
            sender.isSelected == false ? (garden = true) : (garden = false)
        case 14:
            sender.isSelected == false ? (security = true) : (security = false)
        case 15:
            sender.isSelected == false ? (electricity = true) : (electricity = false)
        case 16:
            sender.isSelected == false ? (phone = true) : (phone = false)
        case 17:
            sender.isSelected == false ? (balcony = true) : (balcony = false)
        case 18:
            sender.isSelected == false ? (dressingRoom = true) : (dressingRoom = false)
        default:
            break
        }
        sender.isSelected == false ? (sender.isSelected = true) : (sender.isSelected = false)
    }
    
    @IBAction func acceptBathrooms(_ sender: UIButton) {
        let indexPath = IndexPath(row: 2, section: 0)
        if let visibleIndexPaths = searchTable.indexPathsForVisibleRows?.index(of: indexPath) {
            if visibleIndexPaths != NSNotFound {
                searchTable.reloadRows(at: [indexPath], with: .fade)
                self.selectionView.fadeOutCalls(completion: {
                    (finished: Bool) -> Void in
                    self.selectionView.fadeIn()
                })
                delay(0.6){
                    self.selectionView.isHidden = true
                    self.bathroomsView.isHidden = true
                }
            }
        }
    }
    
    @IBAction func acceptBedrooms(_ sender: UIButton) {
        let indexPath = IndexPath(row: 3, section: 0)
        let contentOffset = searchTable.contentOffset
        if let visibleIndexPaths = searchTable.indexPathsForVisibleRows?.index(of: indexPath) {
            if visibleIndexPaths != NSNotFound {
                searchTable.reloadRows(at: [indexPath], with: .none)
                searchTable.contentOffset = contentOffset
                self.selectionView.fadeOutCalls(completion: {
                    (finished: Bool) -> Void in
                    self.selectionView.fadeIn()
                })
                delay(0.6){
                    self.selectionView.isHidden = true
                    self.bedroomsView.isHidden = true
                }
            }
        }
    }
    
    @IBAction func acceptLocation(_ sender: UIButton) {
        let indexPath = IndexPath(row: 4, section: 0)
        let contentOffset = searchTable.contentOffset
        if let visibleIndexPaths = searchTable.indexPathsForVisibleRows?.index(of: indexPath) {
            if visibleIndexPaths != NSNotFound {
                searchTable.reloadRows(at: [indexPath], with: .none)
                searchTable.contentOffset = contentOffset
                self.selectionView.fadeOutCalls(completion: {
                    (finished: Bool) -> Void in
                    self.selectionView.fadeIn()
                })
                delay(0.6){
                    self.selectionView.isHidden = true
                    self.locationView.isHidden = true
                }
            }
        }
    }
    
    @IBAction func acceptPrice(_ sender: UIButton) {
        let indexPath = IndexPath(row: 6, section: 0)
        let contentOffset = searchTable.contentOffset
        if let visibleIndexPaths = searchTable.indexPathsForVisibleRows?.index(of: indexPath) {
            if visibleIndexPaths != NSNotFound {
                searchTable.reloadRows(at: [indexPath], with: .none)
                searchTable.contentOffset = contentOffset
                self.selectionView.fadeOutCalls(completion: {
                    (finished: Bool) -> Void in
                    self.selectionView.fadeIn()
                })
                delay(0.6){
                    self.selectionView.isHidden = true
                    self.priceView.isHidden = true
                }
            }
        }
    }

    @IBAction func clearFiltersButton(_ sender: UIButton) {
        bathroomSelected = "Elegir"
        bedroomSelected = "Elegir"
        locationSelected = "Elegir"
        priceSelected = "Sin especificar"
        urlParameters = "?"
        rentChecked = false; saleChecked = false; houseChecked = false; deptoChecked = false; terrainChecked = false; bathroomChecked = 0; bedroomChecked = 0; locationChecked = 0; heating = false; chimney = false; internet = false; phone = false; balcony = false; garden = false; tv = false; pool = false; security = false; ac = false; garage = false; serviceRoom = false; laundry = false; electricity = false; elevator = false; naturalGas = false; dressingRoom = false; parking = false; minPrice = 0; maxPrice = 0; priceChecked = 0
        searchTable.reloadData()
    }
    
    @IBAction func applyButton(_ sender: AnyObject) {
        loadingView.isHidden = false
        urlParameters = "?"
        createURL()
    }
    
    func createURL() {
        if rentChecked == false && saleChecked == false {
            self.preloaderIndicator.stopAnimating()
            _ = SweetAlert().showAlert("Datos inválidos!", subTitle: "Debe elegir por lo menos un parámetro(renta/venta).", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    self.loadingView.isHidden = true
                    return
                }
            }
        } else {
            rentChecked == false ? (urlParameters += "") : (urlParameters += "rent=true&")
            saleChecked == false ? (urlParameters += "") : (urlParameters += "sale=true&")
            houseChecked == false ? (urlParameters += "") : (urlParameters += "house=true&")
            deptoChecked == false ? (urlParameters += "") : (urlParameters += "depto=true&")
            terrainChecked == false ? (urlParameters += "") : (urlParameters += "terrain=true&")
            bathroomChecked == 0 ? (urlParameters += "") : (urlParameters += "bathrooms=\(bathroomChecked)&")
            bedroomChecked == 0 ? (urlParameters += "") : (urlParameters += "rooms=\(bedroomChecked)&")
            locationChecked == 0 ? (urlParameters += "") : (addStateToURL(locationChecked))
            priceChecked == 0 ? (urlParameters += "") : (addPriceToURL(priceChecked))
            
            let amenitiesDictionary : NSDictionary = [ "heating" : heating, "chimney" : chimney, "internet" : internet, "phone" : phone, "balcony" : balcony, "garden" : garden, "tv" : tv, "pool" : pool, "security" : security, "ac" : ac, "garage" : garage, "serviceRoom" : serviceRoom, "laundry" : laundry, "electricity" : electricity, "elevator" : elevator, "naturalGas" : naturalGas, "dressingRoom" : dressingRoom, "parking" : parking]
                
            for (key, value) in amenitiesDictionary {
                    value as! Bool == false ? (urlParameters += "") : (urlParameters += "\(key)=true&")
            }
                
            searchRequest()
        }
    }
    
    func addStateToURL(_ stateSelected: Int) {
        for (index, value) in locationsNumber.enumerated() {
            if index == stateSelected {
                urlParameters += "state=\(value)&"
                return
            }
        }
    }
    
    func addPriceToURL(_ priceSelected: Int) {
        switch priceSelected {
        case 1:
            urlParameters += "minPrice=0&maxPrice=500000&"
        case 2:
            urlParameters += "minPrice=500001&maxPrice=1000000&"
        case 3:
            urlParameters += "minPrice=1000001&maxPrice=2000000&"
        case 4:
            urlParameters += "minPrice=2000001&maxPrice=3000000&"
        case 5:
            urlParameters += "minPrice=3000001&maxPrice=4000000&"
        case 6:
            urlParameters += "minPrice=4000001&maxPrice=5000000&"
        case 7:
            urlParameters += "minPrice=5000001&maxPrice=6000000&"
        case 8:
            urlParameters += "minPrice=6000001&maxPrice=7000000&"
        case 9:
            urlParameters += "minPrice=7000001&maxPrice=8000000&"
        case 10:
            urlParameters += "minPrice=8000001&"
        default:
            break;
        }
    }
    
    func searchRequest() {
        preloaderIndicator.startAnimating()
        let urlParametersEncoding = urlParameters.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        request("http://hausem.com/api/properties\(urlParametersEncoding)", method: .post, encoding: URLEncoding.default)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let JSON = response.result.value {
                        searchResults = JSON as! NSArray
                        if searchResults.count == 0 {
                            self.preloaderIndicator.stopAnimating()
                            _ = SweetAlert().showAlert("NO hay resultados!", subTitle: "No hay resultados con estos parámetros.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8)) { (isOtherButton) -> Void in
                                if isOtherButton == true {
                                    self.loadingView.isHidden = true
                                }
                            }
                        } else {
                            self.loadRealEstates()
                        }
                    }
                case .failure(let error):
                    print(error)
                    self.preloaderIndicator.stopAnimating()
                    _ = SweetAlert().showAlert("Error en servidor!", subTitle: "No se estableció una conexión con el servidor.", style: AlertStyle.customImag(imageFile: "error_outline_icon.png"),  buttonTitle:"Aceptar", buttonColor:UIColor.colorFromRGB(0x11B1A8)) { (isOtherButton) -> Void in
                        if isOtherButton == true {
                            self.loadingView.isHidden = true
                        }
                   }
                }
            }
    }
    
    func loadRealEstates() {
        for realEstates in (searchResults as? [[String:Any]])! {
            if let imageRealEstate = realEstates["inmo_main_pic"] as? String {
                if imageRealEstate == "Hausem International" {
                    realEstatesIcon.append("")
                } else {
                    let imageEncoding = imageRealEstate.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    realEstatesIcon.append(imageEncoding)
                }
            }
            if let nameRealEstate = realEstates["inmo_name"] as? String {
                realEstatesName.append(nameRealEstate)
            }
            if let addressRealEstate = realEstates["address"] as? String {
                realEstatesAddress.append(addressRealEstate)
            }
            if let products = realEstates["_products"] as? NSArray {
                let totalProducts = products.count
                realEstatesProducts.append("\(totalProducts) propiedad(es)")
            }
        }
        self.performSegue(withIdentifier: "goToRealEstates", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToRealEstates") {
            print("Enter to segue goToRealEstates")
        }
    }

}// END SEARCH VIEW CONTROLLER
