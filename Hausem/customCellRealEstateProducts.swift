//
//  customCellRealEstateProducts.swift
//  Hausem
//
//  Created by Hector on 27/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class customCellRealEstateProducts: UITableViewCell {
    
    //Products
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAddress: UILabel!
    @IBOutlet weak var bathroomLabel: UILabel!
    @IBOutlet weak var bedroomLabel: UILabel!
    @IBOutlet weak var bathroomIcon: UIImageView!
    @IBOutlet weak var bedroomIcon: UIImageView!
    @IBOutlet weak var saleRentLabel: UILabel!
    @IBOutlet weak var productPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productImage.contentMode = UIViewContentMode.scaleAspectFill
        productImage.clipsToBounds = true
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            productName.font = productName.font.withSize(17)
            productAddress.font = productAddress.font.withSize(15)
            bathroomLabel.font = bathroomLabel.font.withSize(17)
            bedroomLabel.font = bedroomLabel.font.withSize(17)
        }
        else if screenHeight == 736 {
            productName.font = productName.font.withSize(18)
            productAddress.font = productAddress.font.withSize(16)
            bathroomLabel.font = bathroomLabel.font.withSize(18)
            bedroomLabel.font = bedroomLabel.font.withSize(18)
        }
    }

}
