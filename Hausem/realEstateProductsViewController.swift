//
//  realEstateProductsViewController.swift
//  Hausem
//
//  Created by Hector on 27/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class realEstateProductsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Header
    @IBOutlet weak var propietiesLabel: UILabel!
    @IBOutlet weak var productsTable: UITableView!
    @IBOutlet weak var realEstateLogo: UIImageView!
    @IBOutlet weak var realEstateName: UILabel!
    @IBOutlet weak var realEstateAddress: UILabel!
    @IBOutlet weak var realEstateGoToProfileLabel: UILabel!
    @IBOutlet weak var goToProfile: UIButton!
    @IBOutlet weak var darkEffectView: UIView!
    @IBOutlet weak var goBackButton: UIButton!
    @IBOutlet weak var goToMapButton: UIButton!
    
    var idRealEstate = 0
    var realEstateInfo: NSDictionary!
    var productTypeSelected = 0
    var productIdSelected = 0
    var productsImages = [String]()
    var productsName = [String]()
    var productsAddress = [String]()
    var productsBathroom = [NSNumber]()
    var productsBedroom = [NSNumber]()
    var productsBathroomIcon = [UIImage]()
    var productsBedroomIcon = [UIImage]()
    var productsSaleRent = [String]()
    var productsPrice = [Float]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        realEstateInfo = searchResults[idRealEstate] as? NSDictionary
        loadProfileInfo()
        loadProductsRealEstate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
        //topMostController()
    }
    
    override func viewDidLayoutSubviews() {
        screenAllSize()
        screenSize()
        darkEffectView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        realEstateLogo.layer.borderWidth = 1.0
        realEstateLogo.layer.masksToBounds = false
        realEstateLogo.layer.borderColor = UIColor.white.cgColor
        realEstateLogo.layer.cornerRadius = realEstateLogo.frame.size.width/2
        realEstateLogo.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.productsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! customCellRealEstateProducts        
        let url = URL(string: productsImages[indexPath.row])
        cell.productImage.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: {(Bool)  in
        } )
        
        cell.productName.text = productsName[indexPath.row]
        cell.productAddress.text = productsAddress[indexPath.row]
        cell.saleRentLabel.text = productsSaleRent[indexPath.row]
        
        let nfPrice = NumberFormatter()
        nfPrice.numberStyle = .currency
        let sPrice = nfPrice.string(from: NSNumber(value: productsPrice[indexPath.row]))
        cell.productPrice.text = sPrice
        
        if productsBathroom[indexPath.row] != 0 {
            cell.bathroomIcon.image = productsBathroomIcon[indexPath.row]
            let nfBathroom = NumberFormatter()
            nfBathroom.numberStyle = .decimal
            let sBathroom = nfBathroom.string(from: productsBathroom[indexPath.row])
            cell.bathroomLabel.text = sBathroom
        } else {
            cell.bathroomLabel.isHidden = true
            cell.bathroomIcon.isHidden = true
        }
        
        if productsBedroom[indexPath.row] != 0 {
            cell.bedroomIcon.image = productsBedroomIcon[indexPath.row]
            let nfBedroom = NumberFormatter()
            nfBedroom.numberStyle = .decimal
            let sBedroom = nfBedroom.string(from: productsBedroom[indexPath.row])
            cell.bedroomLabel.text = sBedroom
        } else {
            cell.bedroomLabel.isHidden = true
            cell.bedroomIcon.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        productTypeSelected = 3
        productIdSelected = indexPath.row
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToProductDetails", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToProductDetails") {
            print("Enter to segue goToProductDetails")
            let viewController = segue.destination as! productDetailsViewController
            viewController.whereItComesFrom = "realEstateProducts"
            viewController.productType = productTypeSelected
            viewController.idProduct = productIdSelected
            viewController.idRealEstate = idRealEstate
        }
        if (segue.identifier == "goToRealEstateDetails") {
            print("Enter to segue goToRealEstateDetails")
            let viewController = segue.destination as! realEstateDetailViewController
            viewController.idRealEstate = idRealEstate
        }
        if (segue.identifier == "goToMap") {
            print("Enter to segue goToMap")
            let viewController = segue.destination as! mapViewController
            viewController.idRealEstate = idRealEstate
        }
    }
    
    func screenSize() {
        //check iphone's version
        if screenHeight == 667 {
            propietiesLabel.font = propietiesLabel.font.withSize(20)
            realEstateName.font = realEstateName.font.withSize(17)
            realEstateAddress.font = realEstateAddress.font.withSize(15)
            realEstateGoToProfileLabel.font = realEstateGoToProfileLabel.font.withSize(17)
        }
        else if screenHeight == 736 {
            propietiesLabel.font = propietiesLabel.font.withSize(22)
            realEstateName.font = realEstateName.font.withSize(18)
            realEstateAddress.font = realEstateAddress.font.withSize(16)
            realEstateGoToProfileLabel.font = realEstateGoToProfileLabel.font.withSize(17)
        }
    }
    
    @IBAction func goBackButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToRealEstates", sender: self)
        }
    }
    
    @IBAction func goToMapButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToMap", sender: self)
        }
    }
    
    @IBAction func goToProfile(_ sender: UIButton) {
        DispatchQueue.main.async {
            [unowned self] in
            self.performSegue(withIdentifier: "goToRealEstateDetails", sender: self)
        }
    }
    
    func loadProfileInfo() {
        if let imageRealEstate = realEstateInfo!["inmo_main_pic"] as? String {
            let imageEncoding = imageRealEstate.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imageEncoding)
            realEstateLogo.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: {(Bool)  in
            } )
        }
        if let nameRealEstate = realEstateInfo!["inmo_name"] as? String {
            realEstateName.text = nameRealEstate
        }
        if let addressRealEstate = realEstateInfo!["address"] as? String {
            realEstateAddress.text = addressRealEstate
        }
    }
    
    func loadProductsRealEstate() {
        if let products = realEstateInfo!["_products"] as? NSArray {
            for product in (products as? [[String:Any]])! {
                if let image = product["product_main_pic"] as? String {
                    let imageEncoding = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    productsImages.append(imageEncoding)
                }
                if let name = product["product_title"] as? String {
                    productsName.append(name)
                    productsBathroomIcon.append(UIImage(named: "bathroom_icon_outline")!)
                    productsBedroomIcon.append(UIImage(named: "bedroom_icon_outline")!)
                }
                if let price = product["product_price"] as? Float {
                    productsPrice.append(price)
                }
                if let address = product["product_state"] as? String {
                    productsAddress.append(address)
                }
                if let sale = product["product_sale"] as? Bool , let rent = product["product_rent"] as? Bool {
                    let rv = sale == true && rent == true ? "RENTA/VENTA" : sale == true && rent == false ? "VENTA" : sale == false && rent == true ? "RENTA" : "S/D"
                    productsSaleRent.append(rv)
                }
                if let amenities = product["_amenities"] as? NSDictionary {
                    if let bathrooms = amenities.object(forKey: "bathroom") as? NSNumber {
                        productsBathroom.append(bathrooms)
                    } else {
                        productsBathroom.append(0)
                    }
                    if let bedrooms = amenities.object(forKey: "room_number") as? NSNumber {
                        productsBedroom.append(bedrooms)
                    } else {
                        productsBedroom.append(0)
                    }
                }
            }
        }
    }

}//END 
