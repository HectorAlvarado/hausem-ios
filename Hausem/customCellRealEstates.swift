//
//  customCellRealEstates.swift
//  Hausem
//
//  Created by Hector on 26/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class customCellRealEstates: UITableViewCell {
    
    @IBOutlet weak var realEstateLogo: UIImageView!
    @IBOutlet weak var realEstateName: UILabel!
    @IBOutlet weak var realEstateAddress: UILabel!
    @IBOutlet weak var realEstateProducts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
        realEstateLogo.layer.borderWidth = 1.0
        realEstateLogo.layer.masksToBounds = false
        realEstateLogo.layer.borderColor = UIColor.black.cgColor
        realEstateLogo.layer.cornerRadius = realEstateLogo.frame.size.width/2
        realEstateLogo.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            realEstateName.font = realEstateName.font.withSize(17)
            realEstateAddress.font = realEstateAddress.font.withSize(15)
            realEstateProducts.font = realEstateProducts.font.withSize(15)
        }
        else if screenHeight == 736 {
            realEstateName.font = realEstateName.font.withSize(18)
            realEstateAddress.font = realEstateAddress.font.withSize(16)
            realEstateProducts.font = realEstateProducts.font.withSize(16)
        }
    }

}
