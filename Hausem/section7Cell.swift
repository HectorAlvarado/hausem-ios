//
//  section7Cell.swift
//  Hausem
//
//  Created by Hector on 07/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class section7Cell: UITableViewCell {

    @IBOutlet weak var titleSection7: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pricelabel2: UILabel!
    @IBOutlet weak var priceButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            titleSection7.font = titleSection7.font.withSize(15)
        }
        else if screenHeight == 736 {
            titleSection7.font = titleSection7.font.withSize(16)
        }
    }

}
