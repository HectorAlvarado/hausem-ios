//
//  locationCell.swift
//  Hausem
//
//  Created by Hector on 08/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class locationCell: UITableViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            locationLabel.font = locationLabel.font.withSize(16)
        }
        else if screenHeight == 736 {
            locationLabel.font = locationLabel.font.withSize(17)
        }
    }

}
