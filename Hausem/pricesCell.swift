//
//  pricesCell.swift
//  Hausem
//
//  Created by Hector on 14/09/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class pricesCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            priceLabel.font = priceLabel.font.withSize(16)
        }
        else if screenHeight == 736 {
            priceLabel.font = priceLabel.font.withSize(17)
        }
    }

}
