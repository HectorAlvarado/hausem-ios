//
//  collectionCell.swift
//  Hausem
//
//  Created by Hector on 25/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class collectionCell: UICollectionViewCell {
 
    //All views on collection view
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view2_1: UIView!
    @IBOutlet weak var view2_2: UIView!
    
    //Products 
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAddress: UILabel!
    @IBOutlet weak var bathroomLabel: UILabel!
    @IBOutlet weak var bedroomLabel: UILabel!
    @IBOutlet weak var bathroomIcon: UIImageView!
    @IBOutlet weak var bedroomIcon: UIImageView!
    @IBOutlet weak var saleRentLabel: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productImage.contentMode = UIViewContentMode.scaleAspectFill
        productImage.clipsToBounds = true
    }
}
