//
//  bedroomsCell.swift
//  Hausem
//
//  Created by Hector on 08/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class bedroomsCell: UITableViewCell {

    @IBOutlet weak var bedroomsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            bedroomsLabel.font = bedroomsLabel.font.withSize(16)
        }
        else if screenHeight == 736 {
            bedroomsLabel.font = bedroomsLabel.font.withSize(17)
        }
    }

}
