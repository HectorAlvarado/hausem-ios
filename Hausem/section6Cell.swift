//
//  section6Cell.swift
//  Hausem
//
//  Created by Hector on 05/08/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import UIKit

class section6Cell: UITableViewCell {
    
    //buttons
    @IBOutlet weak var titleSection6: UILabel!
    @IBOutlet weak var amenity1Button: UIButton!
    @IBOutlet weak var amenity2Button: UIButton!
    @IBOutlet weak var amenity3Button: UIButton!
    @IBOutlet weak var amenity4Button: UIButton!
    @IBOutlet weak var amenity5Button: UIButton!
    @IBOutlet weak var amenity6Button: UIButton!
    @IBOutlet weak var amenity7Button: UIButton!
    @IBOutlet weak var amenity8Button: UIButton!
    @IBOutlet weak var amenity9Button: UIButton!
    @IBOutlet weak var amenity10Button: UIButton!
    @IBOutlet weak var amenity11Button: UIButton!
    @IBOutlet weak var amenity12Button: UIButton!
    @IBOutlet weak var amenity13Button: UIButton!
    @IBOutlet weak var amenity14Button: UIButton!
    @IBOutlet weak var amenity15Button: UIButton!
    @IBOutlet weak var amenity16Button: UIButton!
    @IBOutlet weak var amenity17Button: UIButton!
    @IBOutlet weak var amenity18Button: UIButton!
    
    //labels
    @IBOutlet weak var poolLabel: UILabel!
    @IBOutlet weak var acLabel: UILabel!
    @IBOutlet weak var tvcableLabel: UILabel!
    @IBOutlet weak var heatingLabel: UILabel!
    @IBOutlet weak var chimneyLabel: UILabel!
    @IBOutlet weak var laundryRoomLabel: UILabel!
    @IBOutlet weak var serviceRoomLabel: UILabel!
    @IBOutlet weak var elevatorLabel: UILabel!
    @IBOutlet weak var parkingLabel: UILabel!
    @IBOutlet weak var garageLabel: UILabel!
    @IBOutlet weak var naturalGasLabel: UILabel!
    @IBOutlet weak var internetLabel: UILabel!
    @IBOutlet weak var gardenLabel: UILabel!
    @IBOutlet weak var securityLabel: UILabel!
    @IBOutlet weak var electricityLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var terraceLabel: UILabel!
    @IBOutlet weak var dressingRoomLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        screenAllSize()
        screenSize()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func screenSize() {
        if screenHeight == 667 {
            titleSection6.font = titleSection6.font.withSize(15)
        }
        else if screenHeight == 736 {
            titleSection6.font = titleSection6.font.withSize(16)
        }
    }

}
