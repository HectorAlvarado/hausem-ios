//
//  general.swift
//  Hausem
//
//  Created by Hector on 15/07/16.
//  Copyright © 2016 Hector Alvarado. All rights reserved.
//

import Foundation
import UIKit

/*Global's variables*/
var startController = ViewController()
/*END Global's variables*/

/*Screen sizes*/
var screenHeight: CGFloat! = 0
var screenWidth: CGFloat! = 0
/*END of Screen sizes*/

/*GLOBAL JSON WEB SERVICES TOP 10*/
var top10Dictionary: NSDictionary!
var top10Houses: NSArray!
var top10Deptos: NSArray!
var top10Terrains: NSArray!

/*HOUSES*/
var productsImagesHouses = [String]()
var productsNameHouses = [String]()
var productsPriceHouses = [Float]()
var productsAddressHouses = [String]()
var productsBathroomHouses = [NSNumber]()
var productsBedroomHouses = [NSNumber]()
var productsSaleRentHouses = [String]()
var productsBathroomIconHouses = [UIImage]()
var productsBedroomIconHouses = [UIImage]()
/*HOUSES*/

/*DEPTOS*/
var productsImagesDeptos = [String]()
var productsNameDeptos = [String]()
var productsPriceDeptos = [Float]()
var productsAddressDeptos = [String]()
var productsBathroomDeptos = [NSNumber]()
var productsBedroomDeptos = [NSNumber]()
var productsSaleRentDeptos = [String]()
var productsBathroomIconDeptos = [UIImage]()
var productsBedroomIconDeptos = [UIImage]()
/*DEPTOS*/

/*TERRAINS*/
var productsImagesTerrains = [String]()
var productsNameTerrains = [String]()
var productsPriceTerrains = [Float]()
var productsAddressTerrains = [String]()
var productsBathroomTerrains = [NSNumber]()
var productsBedroomTerrains = [NSNumber]()
var productsSaleRentTerrains = [String]()
var productsBathroomIconTerrains = [UIImage]()
var productsBedroomIconTerrains = [UIImage]()
/*TERRAINS*/
/*END GLOBAL JSON WEB SERVICES TOP 10*/

/*GLOBAL JSON PROPIERTIES*/
var searchResults: NSArray!

//GET REAL ESTATES
var realEstatesIcon = [String]()
var realEstatesName = [String]()
var realEstatesAddress = [String]()
var realEstatesProducts = [String]()

/*END GLOBAL JSON PROPIERTIES*/


extension UIView {
    func fadeIn(_ duration: TimeInterval = 0.6, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)
    }
    
    func fadeOut(_ duration: TimeInterval = 0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
    
    func fadeOutCalls(_ duration: TimeInterval = 0.6, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}

//Check the size of the screen sidth/heigth
func screenAllSize() {
    let screenSize: CGRect = UIScreen.main.bounds
    screenHeight = screenSize.height
    screenWidth = screenSize.width
}

//This is a delay function
func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func localNotification(_ message:String, title:String) {
    let localNotification = UILocalNotification()
    localNotification.fireDate = Date(timeIntervalSinceNow: 5)
    localNotification.alertBody = message
    localNotification.alertAction = title
    localNotification.timeZone = TimeZone.current
    localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
    
    UIApplication.shared.scheduleLocalNotification(localNotification)
}


func JSONStringify(_ value: AnyObject,prettyPrinted:Bool = false) -> String{
    let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    if JSONSerialization.isValidJSONObject(value) {
        do{
            let data = try JSONSerialization.data(withJSONObject: value, options: options)
            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return string as String
            }
        }catch {
            print("error stringify")
            //Access error here
        }
    }
    return ""
    
}

func topMostController() -> UIViewController {
    var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
    while (topController.presentedViewController != nil) {
        topController = topController.presentedViewController!
    }
    return topController
}

func resetSearchResults() {
    //RESET SEARCH
    searchResults = []
    realEstatesIcon = []
    realEstatesName = []
    realEstatesAddress = []
    realEstatesProducts = []
}
